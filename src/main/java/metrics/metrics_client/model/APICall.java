package metrics.metrics_client.model;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import metrics.metrics_client.interfaces.IAPICall;

public class APICall implements IAPICall {
	private RestTemplate restTemplate = null;
	private HttpEntity<MultiValueMap<String, Object>> requestEntity;
	
	public APICall(RestTemplate restTemplate, HttpEntity<MultiValueMap<String, Object>> requestEntity) {
		this.restTemplate = restTemplate;
		this.requestEntity = requestEntity;
	}
	
	public byte[] call(String URI) {
		System.out.println("Calling: " + URI);
		ResponseEntity<byte[]> response = null;
		try {
			response = restTemplate
					  .postForEntity(URI, requestEntity, byte[].class);
			HttpStatus status = response.getStatusCode();
			System.out.print("(" + status.toString() + ") - ");
			if (HttpStatus.OK.equals(status)) {
				System.out.println("Call performed correctly");
			} else {
				System.err.println("Call error");
			}
		} catch (HttpServerErrorException e) {
			System.out.println("(Error) HTTP status: " + e.getMessage());
			return null;
		}
		
		return response.getBody();
	}
	
}
