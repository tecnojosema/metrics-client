package metrics.metrics_client.model;

import java.io.File;

import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

public class RequestEntity {
	private File file;
	private String email;
	private Integer kValue;
	
	public RequestEntity(File file, String email, Integer kValue) {
		this.file = file;
		this.email = email;
		this.kValue = 5;	//Default
		
		if (kValue != null) {
			this.kValue = kValue;
		}
	}
	
	public HttpEntity<MultiValueMap<String, Object>> getRequest() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();
		body.add("file", new FileSystemResource(file));
		if (email != null && !email.isEmpty()) {
			body.add("email", email);
		}
		if (kValue != null) {
			body.add("runK", kValue);
		}
		HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<MultiValueMap<String, Object>>(body, headers);
		return requestEntity;
	}

}
