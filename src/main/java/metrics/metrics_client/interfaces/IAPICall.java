package metrics.metrics_client.interfaces;


public interface IAPICall {
	public byte[] call(String URI);
}
