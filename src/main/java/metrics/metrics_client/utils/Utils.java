package metrics.metrics_client.utils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
	
	public static boolean isEmail(String email) {
		Pattern pattern = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");
	 	Matcher mat = pattern.matcher(email);
		
		if(mat.matches()){
			return true;
		}
		return false;
	}
	
	public static void writeBytes(byte[] data, String outputName) throws FileNotFoundException, IOException {
		if (data != null) {
			try (FileOutputStream fos = new FileOutputStream(outputName)) {
			   fos.write(data);
			} // fos instance is inside the try(), it will be automatically closed afterwards
			System.out.println("Data saved in '" + outputName + "'");
		}
	}
}
