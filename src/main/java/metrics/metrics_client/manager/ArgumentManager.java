package metrics.metrics_client.manager;

import java.io.File;
import java.security.InvalidParameterException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import metrics.metrics_client.App;
import metrics.metrics_client.utils.Utils;

public class ArgumentManager {
	public final static String HELP_S = "h";
	public final static String HELP_L = "help";
	public final static String INPUT_FILE_S = "f";
	public final static String INPUT_FILE_L = "file";
	public final static String EMAIL_S = "e";
	public final static String EMAIL_L = "email";
	public final static String KVALUE_S = "k";
	public final static String KVALUE_L = "kvalue";
	
	private static ArgumentManager instance = null;
	private Options argumentOptions = new Options();
	private CommandLine cmd = null;
	
	private ArgumentManager() {
		Option input = new Option(INPUT_FILE_S, INPUT_FILE_L, true, "input path to csv file");
        input.setRequired(true);
        argumentOptions.addOption(input);

        Option email = new Option(EMAIL_S, EMAIL_L, true, "email where data will be sent");
        email.setRequired(false);
        argumentOptions.addOption(email);
        
        Option kValue = new Option(KVALUE_S, KVALUE_L, true, "Cluster K value");
        kValue.setRequired(false);
        argumentOptions.addOption(kValue);
        
        
        Option help = new Option(HELP_S, HELP_L, true, "help message");
        help.setRequired(false);
        argumentOptions.addOption(help);
	}
	
	public static synchronized ArgumentManager getInstance() {
		if (instance != null) {
			return instance;
		}
		instance = new ArgumentManager();
		return instance;
	}
	
	public void parse(String[] args) {
		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		try {
            cmd = parser.parse(argumentOptions, args);
		} catch (ParseException e) {
		    System.out.println(e.getMessage());
		    formatter.printHelp(App.APP_NAME, argumentOptions);
		    System.exit(1);
		}
		if (cmd.hasOption(HELP_S)) {
			formatter.printHelp(App.APP_NAME, argumentOptions);
		}
	}
	
	public void checkArguments() {
		String inputPath = getInputPath();
		File inputFile = new File(inputPath);
		if (!inputFile.exists()) {
			throw new IllegalArgumentException("Input file '" + inputPath + "' does not exist");
		}
		String email = getEmail();
		if (email != null && !Utils.isEmail(email)) {
			throw new IllegalArgumentException("Email '" + email + "' is not an email");
		}
		
		Integer kValue = getKValue();
		if (kValue != null && (kValue < 2 || kValue > 15)) {
			throw new IllegalArgumentException("KValue '" + kValue + "' must be in range [2,15]");
		}
	}
	
	public String getInputPath() {
		return getOptionValue(INPUT_FILE_S);
	}
	
	public String getEmail() {
		return getOptionValue(EMAIL_S);
	}
	
	public Integer getKValue() {
		String strKValue = getOptionValue(KVALUE_S);
		if (strKValue != null) {
			return Integer.parseInt(strKValue);
		}
		return null;
	}
	
	
	public String getOptionValue(String parameter) {
		if (cmd != null) {
			return cmd.getOptionValue(parameter);
		}
		return null;
	}
	
	public Boolean hasOption(String parameter) {
		if (cmd != null) {
			return cmd.hasOption(parameter);
		}
		return false;
	}
	
}
