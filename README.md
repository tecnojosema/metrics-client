## What is this? ##
An example client app that runs API calls from the project http://sele.inf.um.es/evaluome/.
You can find released builds at http://sele.inf.um.es/evaluome/releases/.

## How to build the project ##
Compile project using `mvn install`. The JAR file will be placed in 'target/' folder, the executable is named 'metrics-client.jar'.

## Input examples ##
Input CSV files are provided in the [files](src/main/resources/files) directory in this repository.

## Run examples ##

* `java -jar metrics-client.jar -f metrics.csv`
	This execution results in 3 API calls:
	+ `http://semantics.inf.um.es:8080/metrics-ws/rest/stabilityIndexCSV`
	+ `http://semantics.inf.um.es:8080/metrics-ws/rest/qualityIndicesCSV`
	+ `http://semantics.inf.um.es:8080/metrics-ws/rest/correlation`
	(Each call generates local zip file in the current folder where the app is running).

* `java -jar metrics-client.jar -f agro.csv -e email@email.com`
This performs the same API calls described above, however, the results will be sent through emails which contain the download links.

* `java -jar metrics-client.jar -f metrics.csv -k 4`
Changing the cluster group division to 4 in order to group data into 4 clusters. The default *k* value is 5 if none is specified.

Note: The CSV file format is described in [our web](http://sele.inf.um.es/evaluome/help.html)