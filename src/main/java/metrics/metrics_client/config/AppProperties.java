package metrics.metrics_client.config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class AppProperties {
	public final static String URI_STABILITY_INDEX_CSV = "URI_STABILITY_INDEX_CSV";
	public final static String URI_STABILITY_INDEX_CSV_EMAIL = "URI_STABILITY_INDEX_CSV_EMAIL";
	public final static String URI_QUALITY_INDICES_CSV = "URI_QUALITY_INDICES_CSV";
	public final static String URI_QUALITY_INDICES_CSV_EMAIL = "URI_QUALITY_INDICES_CSV_EMAIL";
	public final static String URI_CORRELATION = "URI_CORRELATION";
	public final static String URI_CORRELATION_EMAIL = "URI_CORRELATION_EMAIL";
	
	private final static String PROP_CONFIG_FILENAME = "config.properties";
	private InputStream inputStream;
	
	public Properties getPropValues() throws IOException {
		Properties prop = null;
		prop = new Properties();
		inputStream = getClass().getClassLoader().getResourceAsStream(PROP_CONFIG_FILENAME);
 
		if (inputStream != null) {
			prop.load(inputStream);
		} else {
			throw new FileNotFoundException("Property file '" + PROP_CONFIG_FILENAME + "' not found in the classpath");
		}
 
		return prop;
	}
	
}
