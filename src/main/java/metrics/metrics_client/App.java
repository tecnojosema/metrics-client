package metrics.metrics_client;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import metrics.metrics_client.config.AppConfig;
import metrics.metrics_client.config.AppProperties;
import metrics.metrics_client.manager.ArgumentManager;
import metrics.metrics_client.model.APICall;
import metrics.metrics_client.model.RequestEntity;
import metrics.metrics_client.utils.Utils;

public class App {
	public static String APP_NAME = "metrics-client";
	private static ArgumentManager argManager = ArgumentManager.getInstance();
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
		AppProperties appProperties = new AppProperties();
		Properties properties = appProperties.getPropValues();
		
		RestTemplate restTemplate = applicationContext.getBean(RestTemplate.class);
		argManager.parse(args);
		argManager.checkArguments();
		
		File file = new File(argManager.getInputPath());
		String email = argManager.getEmail();
		Integer kValue = argManager.getKValue();
		
		RequestEntity request = new RequestEntity(file, email, kValue);

		byte[] response = null;
		APICall apiCall = new APICall(restTemplate, request.getRequest());
		
		//API Calls
		
		if (email == null) {
			response = apiCall.call(properties.getProperty(AppProperties.URI_STABILITY_INDEX_CSV));
			Utils.writeBytes(response, "stability-index.zip");
			response = apiCall.call(properties.getProperty(AppProperties.URI_QUALITY_INDICES_CSV));
			Utils.writeBytes(response, "quality-indices.zip");
			response = apiCall.call(properties.getProperty(AppProperties.URI_CORRELATION));
			Utils.writeBytes(response, "correlation.zip");
		} else {
			apiCall.call(properties.getProperty(AppProperties.URI_STABILITY_INDEX_CSV_EMAIL));
			apiCall.call(properties.getProperty(AppProperties.URI_QUALITY_INDICES_CSV_EMAIL));
			apiCall.call(properties.getProperty(AppProperties.URI_CORRELATION_EMAIL));
			System.out.println("Download links sent to: '" + email + "'");
		}
		
	}
}
